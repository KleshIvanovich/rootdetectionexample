package com.verkhovsky.research.rootdetection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.verkhovsky.research.magiskdetector.MagiskDetector;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView resultView = findViewById(R.id.result);
        resultView.setText(MagiskDetector.isRooted(this) ? "Rooted!" : "Seem not to be rooted");
    }
}
