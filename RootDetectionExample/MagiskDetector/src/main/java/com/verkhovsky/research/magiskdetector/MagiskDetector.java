package com.verkhovsky.research.magiskdetector;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MagiskDetector {

    private static Context mContext;

    public static boolean isRooted(Context context) {
        mContext = context;
        return detectRootManagementApps() || detectPotentiallyDangerousApps() || checkForSuBinary()
                || checkForDangerousProps() || checkForRWPaths() || checkForRegularExpressions()
                || detectTestKeys() || checkSuExists() || checkForMagiskBinary();
    }

    private static boolean checkForRegularExpressions() {
        List<ApplicationInfo> installedApps = mContext
                .getPackageManager()
                .getInstalledApplications(PackageManager.GET_META_DATA);

        for (ApplicationInfo appInfo : installedApps) {
            for (String regex : Constants.regularExpr)
            if (appInfo.packageName.contains(regex)) {
                return true;
            }
        }
        return false;
    }

    private static boolean detectTestKeys() {
        String buildTags = android.os.Build.TAGS;

        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean detectRootManagementApps() {
        return detectRootManagementApps(null);
    }


    private static boolean detectRootManagementApps(String[] additionalRootManagementApps) {

        // Create a list of package names to iterate over from constants any others provided
        ArrayList<String> packages = new ArrayList<>();
        packages.addAll(Arrays.asList(Constants.knownRootAppsPackages));
        if (additionalRootManagementApps != null && additionalRootManagementApps.length > 0) {
            packages.addAll(Arrays.asList(additionalRootManagementApps));
        }

        return isAnyPackageFromListInstalled(packages);
    }


    private static boolean detectPotentiallyDangerousApps() {
        return detectPotentiallyDangerousApps(null);
    }

    private static boolean detectPotentiallyDangerousApps(String[] additionalDangerousApps) {

        // Create a list of package names to iterate over from constants any others provided
        ArrayList<String> packages = new ArrayList<>();
        packages.addAll(Arrays.asList(Constants.knownDangerousAppsPackages));
        if (additionalDangerousApps != null && additionalDangerousApps.length > 0) {
            packages.addAll(Arrays.asList(additionalDangerousApps));
        }

        return isAnyPackageFromListInstalled(packages);
    }


    private static boolean detectRootCloakingApps(String[] additionalRootCloakingApps) {

        ArrayList<String> packages = new ArrayList<>();
        packages.addAll(Arrays.asList(Constants.knownRootCloakingPackages));
        if (additionalRootCloakingApps != null && additionalRootCloakingApps.length > 0) {
            packages.addAll(Arrays.asList(additionalRootCloakingApps));
        }
        return isAnyPackageFromListInstalled(packages);
    }

    private static boolean checkForSuBinary() {
        return checkForBinary("su");
    }

    private static boolean checkForMagiskBinary() {
        return checkForBinary("magisk");
    }

    private static boolean checkForBinary(String filename) {

        String[] pathsArray = Constants.suPaths;

        boolean result = false;

        for (String path : pathsArray) {
            File f = new File(path, filename);
            boolean fileExists = f.exists();
            if (fileExists) {
                result = true;
            }
        }

        return result;
    }

    private static String[] propsReader() {
        try {
            InputStream inputstream = Runtime.getRuntime().exec("getprop").getInputStream();
            if (inputstream == null) return null;
            String propVal = new Scanner(inputstream).useDelimiter("\\A").next();
            return propVal.split("\n");
        } catch (IOException | NoSuchElementException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String[] mountReader() {
        try {
            InputStream inputstream = Runtime.getRuntime().exec("mount").getInputStream();
            if (inputstream == null) return null;
            String propVal = new Scanner(inputstream).useDelimiter("\\A").next();
            return propVal.split("\n");
        } catch (IOException | NoSuchElementException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean isAnyPackageFromListInstalled(List<String> packages) {
        boolean result = false;

        PackageManager pm = mContext.getPackageManager();

        for (String packageName : packages) {
            try {
                // Root app detected
                pm.getPackageInfo(packageName, 0);
                result = true;
            } catch (PackageManager.NameNotFoundException e) {
                // Exception thrown, package is not installed into the system
            }
        }

        return result;
    }

    private static boolean checkForDangerousProps() {

        final Map<String, String> dangerousProps = new HashMap<>();
        dangerousProps.put("ro.debuggable", "1");
        dangerousProps.put("ro.secure", "0");

        boolean result = false;

        String[] lines = propsReader();

        if (lines == null) {
            // Could not read, assume false;
            return result;
        }

        for (String line : lines) {
            for (String key : dangerousProps.keySet()) {
                if (line.contains(key)) {
                    String badValue = dangerousProps.get(key);
                    badValue = "[" + badValue + "]";
                    if (line.contains(badValue)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    private static boolean checkForRWPaths() {

        boolean result = false;

        String[] lines = mountReader();

        if (lines == null) {
            // Could not read, assume false;
            return result;
        }

        for (String line : lines) {

            // Split lines into parts
            String[] args = line.split("");

            if (args.length < 4) {
                // If we don't have enough options per line, skip this and log an error
                continue;
            }

            String mountPoint = args[1];
            String mountOptions = args[3];

            for (String pathToCheck : Constants.pathsThatShouldNotBeWrtiable) {
                if (mountPoint.equalsIgnoreCase(pathToCheck)) {

                    for (String option : mountOptions.split(",")) {

                        if (option.equalsIgnoreCase("rw")) {
                            result = true;
                            break;
                        }
                    }
                }
            }
        }

        return result;
    }

    private static boolean checkSuExists() {

        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"which", "su"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            return in.readLine() != null;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

}
